/**
 *	App.java
 *
 *	Run Simple Symbolic Regression Algorithm
 *
 *	@author Kelvin (Zaw Moe Htat)
 */

import ga.*;
import common.*;
import java.util.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;

public class App {

	public static void main(String[] args) {

		// Print header
		System.out.println("=================================================");
		System.out.println("==         Simple Symbolic Regression          ==");
		System.out.println("=================================================");

		System.out.println();

		// Initailise for keyboard inputs
		Scanner scanner = new Scanner(System.in);

		// Main Menu
		String mainMenuCommand = "";

		do {

			System.out.println("Choose the following options: ");
			System.out.println("1. Start");
			System.out.println("0. Exit");
			System.out.println();

			System.out.print("> ");

			mainMenuCommand = scanner.nextLine();

			System.out.println();

			if (mainMenuCommand.equals("1")) {

				// File name of dataset
				System.out.print("CSV Dataset filename (without file format) > ");
				String filename = scanner.nextLine();

				// Delimiter for csv file
				System.out.print("Delimiter for CSV > ");
				String delimiter = scanner.nextLine();

				Parser csv = null;

				try {
					csv = new Parser(filename, delimiter);
				} catch(IOException e) {
					e.printStackTrace();
				}

				// Inputs from dataset
				LinkedList<Map<String, Double> > inputs = csv.getInputs();

				// Outputs from dataset
				LinkedList<Double> outputs = csv.getOutputs();

				// Labels
				LinkedList<String> labels = csv.getLabels();

				// Generator command
				String generatorCommand = "";

				System.out.println();

				do {

					// Gene Generator Configuration
					GeneGenerator geneGenerator = new GeneGenerator();

					// Add symbols as default
					geneGenerator.addTerminalGeneType(GeneType.SYMBOL);

					System.out.println("Reading data from " + filename + ".csv");
					System.out.println();

					System.out.print("Add mathematical operators? (y/n) > ");
					String addMathOpCommand = scanner.nextLine();

					if (addMathOpCommand.equals("y") || addMathOpCommand.equals("Y")) {
						
						// Add math op to generator config
						geneGenerator.addNonTerminalGeneType(GeneType.MATHOP);

						System.out.println("Enter mathematical operators [+, -, *, /, ^]. (Seperate by space for multiple operators) ");
						System.out.print("> ");

						String[] mathOps = scanner.nextLine().split(" ");

						if (mathOps.length > 0) {
							for(int i = 0; i < mathOps.length; i++) {
								if (!geneGenerator.getMathOperators().contains(MathOperator.encode(mathOps[i]))) {
									geneGenerator.addMathOperator(MathOperator.encode(mathOps[i]));
								}
							}
						}

					}

					System.out.print("Add mathematical functions? (y/n) > ");
					String addMathFuncCommand = scanner.nextLine();

					if (addMathFuncCommand.equals("y") || addMathFuncCommand.equals("Y")) {

						// Add math func to generator config
						geneGenerator.addNonTerminalGeneType(GeneType.MATHFUNC);

						System.out.println("Enter mathematical functions [sin, cos, tan]. (Seperate by space for mulitple functions)");
						System.out.print("> ");

						String[] mathFuncs = scanner.nextLine().split(" ");

						if (mathFuncs.length > 0) {
							for(int i = 0; i < mathFuncs.length; i++) {
								if (!geneGenerator.getMathFunctions().contains(MathFunction.encode(mathFuncs[i].toLowerCase()))) {
									geneGenerator.addMathFunction(MathFunction.encode(mathFuncs[i].toLowerCase()));
								}
							}
						}
					}

					System.out.print("Add integer number? (y/n) > ");
					String addIntegerConstCommand = scanner.nextLine();

					if (addIntegerConstCommand.equals("y") || addIntegerConstCommand.equals("Y")) {
						geneGenerator.addTerminalGeneType(GeneType.INTCONST);
					}

					System.out.print("Add decimal number? (y/n) > ");
					String addDoubleConstCommand = scanner.nextLine();

					if (addDoubleConstCommand.equals("y") || addDoubleConstCommand.equals("Y")) {
						geneGenerator.addTerminalGeneType(GeneType.DOUBLECONST);
					}

					// Add symbols
					for(String symbol : labels) {
						geneGenerator.addSymbol(symbol);
					}

					System.out.print("Enter maximum lenght of gene > ");
					int geneLength = scanner.nextInt();

					System.out.print("Enter number of population > ");
					int numPopulation = scanner.nextInt();

					System.out.print("Enter number of generations > ");
					int numGeneration = scanner.nextInt();


					// Set number of genes to be generated
					geneGenerator.setGeneLength(geneLength);

					// GA Engine
					Engine ga = new Engine();

					ga.setGeneGenerator(geneGenerator);
					ga.setInputs(inputs);
					ga.setLabels(labels);
					ga.setOutputs(outputs);
					ga.initialisePopulation(numPopulation);

					ga.run(numGeneration);

					System.out.println();
					System.out.println("Choose the following options: ");
					System.out.println("1. Start again");
					System.out.println("0. Main Menu");

					scanner.nextLine();

					System.out.print("> ");
					generatorCommand = scanner.nextLine();

					System.out.println();

				} while(!generatorCommand.equals("0"));
			}
		} while(!mainMenuCommand.equals("0"));

	}

}