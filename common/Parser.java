package common;

import java.util.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;

/**
 *	Parser.java
 *
 *	Parse the csv dataset
 *
 *	@author Kelvin (Zaw Moe Htat)
 */

public class Parser {

	// Inputs from dataset
	private LinkedList<Map<String, Double> > inputs;

	// Outputs from dataset
	private LinkedList<Double> outputs;

	// Labels
	private LinkedList<String> labels;

	/**
	 *	Default initialisation
	 */
	public Parser() {
		this.inputs = new LinkedList<Map<String, Double> >();
		this.outputs = new LinkedList<Double>();
		this.labels = new LinkedList<String>();
	}

	/**
	 *	Initialise by parsing data
	 *
	 *	@param filename  Dataset filename
	 *	@param delimiter CSV seperator
	 *
	 *	@throws IOException
	 */
	public Parser(String filename, String delimiter) throws IOException {
		this.inputs = new LinkedList<Map<String, Double> >();
		this.outputs = new LinkedList<Double>();
		this.labels = new LinkedList<String>();
		
		this.parse(filename, delimiter);	
	}

	/**
	 *	Parse data
	 *
	 *	@param filename  Dataset filename
	 *	@param delimiter CSV seperator
	 *
	 *	@throws IOException
	 */
	public void parse(String filename, String delimiter) throws IOException {

		// File path
		String pathToFile = "datasets/" + filename + ".csv";

		// To open file
		FileReader fileReader = new FileReader(pathToFile); 

		// To read file line by line
		BufferedReader bufferedReader  = new BufferedReader(fileReader);

		String line;

		// Row of csv file
		int row = 0;

		while((line = bufferedReader.readLine()) != null) {

			// Store input
			Map<String, Double> input = new HashMap<String, Double>();

			// Make sure no string is empty
			if (line.length() > 0) {

				String[] cols = line.split(delimiter);

				for(int i = 0; i < cols.length; i++) {

					// Labels and inputs are from index 0 to cols.length - 1.
					if (i < (cols.length - 1)) {

						// First row is always labels
						if (row == 0) {
							this.labels.add(cols[i]);
						} else {

							// Add inputs
							input.put(labels.get(i), Double.parseDouble(cols[i]));

						}

					} else {
						if (row > 0) {

							// Add input into input list
							this.inputs.add(input);

							// Add output into output list
							this.outputs.add(Double.parseDouble(cols[i]));
						}
					}

				}

				row++;
			}
		}

		bufferedReader.close();
		fileReader.close();
	}

	/**
	 *	Get labels
	 *
	 *	@return Label of dataset
	 */
	public LinkedList<String> getLabels() {
		return this.labels;
	}

	/**
	 *	Get inputs
	 *
	 *	@return Inputs
	 */
	public LinkedList<Map<String, Double> > getInputs() {
		return this.inputs;
	}

	/**
	 *	Get outputs
	 *
	 *	@return Outputs
	 */
	public LinkedList<Double> getOutputs() {
		return this.outputs;
	}

}