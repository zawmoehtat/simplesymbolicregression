# Simple Symbolic Regression using Genetic Algorithm

## Datasets

Datasets are in CSV file format. The first rows must be labels. Example:

|x|y|outputs|
|---|---|---|
|4|6|52|
|10|2|104|
|1|1|2|
|10|1|101|

## Usage

``` 
javac *.java 
java App
```