package ga;

import java.util.Random;
import java.util.LinkedList;

/**
 *	MathOperator.java
 *
 *	MathOperator handle the execution of two numerical values over
 *	the math operator.
 *
 *	@author Kelvin (Zaw Moe Htat)
 */

public class MathOperator {

	/**
	 *	Index of math operators set
	 */
	public static final int ADD = 0;
	public static final int MUL = 1;
	public static final int DIV = 2;
	public static final int SUB = 3;
	public static final int POW = 4;

	/**
	 *	Math Operators
	 */

	public static String[] operators = {
		"+",	// ADD
		"*",	// MUL
		"/",	// DIV
		"-",	// SUB
		"^"		// POW
	};

	/**
	 *	Encode math operators
	 *
	 *	@param operator
	 *
	 *	@return -1 if not exists, operator id if it does
	 */
	public static int encode(String operator) {
		int id = -1;

		for(int i = 0; i < MathOperator.operators.length; i++) {
			if (MathOperator.operators[i].equals(operator)) {
				id = i;
				break;
			}
		}

		return id;
	}

	/**
	 *	Decode math operators
	 *
	 *	@param id 
	 *
	 *	@return Null if not exists, operator if it does
	 */
	public static String decode(int id) {
		if (id >= operators.length || id < 0) {
			return null;
		}

		return MathOperator.operators[id];

	}

	/**
	 *	Get the random operator
	 *
	 *	@return Random operator from operators set
	 */
	public static String getRandomOperator() {

		// Initialise Random
		Random random = new Random();

		return MathOperator.operators[random.nextInt(MathOperator.operators.length)];
	}

	/**
	 *	Get the selective operator randomly
	 *
	 *	@param selection List of custom selected operators
	 *
	 *	@return Random operator from the selection list
	 */
	public static String getRandomOperator(LinkedList<Integer> selection) {

		// Initialise Random
		Random random = new Random();

		return MathOperator.operators[selection.get(random.nextInt(selection.size()))];

	}

	/**
	 *	Perform mathematical computation
	 *
	 *	@param operator Math operator
	 *	@param a 	    Numeric
	 *	@param b 		Numeric
	 *
	 *	@return Result of computation
	 */
	public static double execute(String operator, double a, double b) {

		// Perform addition
		if (operator.equals("+")) {
			return a + b;
		}

		// Perform multiplication
		if (operator.equals("*")) {
			return a * b;
		}

		// Perform division
		if (operator.equals("/")) {
			return a / b;
		}

		// Perform substraction
		if (operator.equals("-")) {
			return a - b;
		}

		// Perform power
		if (operator.equals("^")) {
			return Math.pow(a, b);
		}

		return 0;

	}

	/**
	 *	Perform mathematical computation with MathOperator index value
	 *
	 *	@param operatorIndex Math operator index
	 *	@param a 	    	 Numeric
	 *	@param b 			 Numeric
	 *
	 *	@return Result of computation
	 */
	public static double execute(int operatorIndex, double a, double b) {
		return MathOperator.execute(MathOperator.operators[operatorIndex], a, b);
	}

}