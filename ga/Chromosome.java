package ga;

import java.util.*;

/**
 *	Chromosome.java
 *
 *	Chromosome is a binary tree that contains genes.
 *
 *	@author Kelvin (Zaw Moe Htat)
 */

public class Chromosome {

	/**
	 *	Mark the gene at the root of the tree
	 */
	private Gene root;

	/**
	 *	Index of gene
	 */
	private int geneIndex;

	/**
	 *	Gene Generator
	 */
	private GeneGenerator geneGenerator;

	/**
	 *	Default Constructor
	 *
	 *	Initialise chromosome
	 */
	public Chromosome() {
		this.root = null;
		this.geneIndex = 0;
		this.geneGenerator = null;
	}

	/**
	 *	Constructor with parameters
	 *
	 *	Initialise genes of chromosome with the given one
	 */
	public Chromosome(Gene gene) {
		this.root = gene;
		this.geneIndex = 0;
		this.geneGenerator = null;
	}

	/**
	 *	Constructor with gene generator
	 */
	public Chromosome(GeneGenerator geneGenerator) {
		this.root = null;
		this.geneIndex = 0;
		this.geneGenerator = geneGenerator;
	}

	/**
	 *	Constructor with parameters
	 *
	 *	Initialise genes of chromosome with the given one
	 */
	public Chromosome(Gene gene, GeneGenerator geneGenerator) {
		this.root = gene;
		this.geneIndex = 0;
		this.geneGenerator = geneGenerator;
	}

	/**
	 *	Initialise chromosome
	 *
	 *	@see init(Generator geneGenerator);
	 */
	public void init() {
		this.init(this.geneGenerator);
	}

	/**
	 *	Initialise chromosome
	 *
	 *	This method initialise chromosomes.
	 *
	 *	Gene length is used as a weight of the gene.
	 *	The random number is generated in the range of
	 *	0 to number of genes. Initially, the half of the
	 *	number of gene is used as the weight of the first gene.
	 *
	 *	After empty genes are generated, the algorithm will fix the
	 *	chromosome if the structue is broken (i.e not Binary Tree).
	 *
	 *	Then, all empty genes will be filled up with allele
	 *
	 *	@param geneGenerator Generator to generate initial genes
	 */
	public void init(GeneGenerator geneGenerator) {

		this.geneGenerator = geneGenerator;

		// Get gene length
		int geneLength = geneGenerator.getGeneLength();

		// Weight of the first gene
		int rootWeight = (geneLength / 2);

		// Add the first empty gene to the chromosome
		Gene newGene = new Gene();
		newGene.setWeight(rootWeight);
		this.root = newGene;

		// Random Engine
		Random random = new Random();

		// Get random weight
		int randomWeight = random.nextInt(geneLength + 1);

		while(randomWeight < geneLength || this.getGeneLength() < 3) {
			
			// Add empty gene to the chromosome
			this.addEmptyGeneRecursive(this.root, randomWeight);

			// Get random weight
			randomWeight = random.nextInt(geneLength + 1);
		}

		// Amendment
		// Fix the broken binary tree
		// Tree might not be well constructed.
		this.amendment(this.root);

		// Initialise root first
		if (geneGenerator.getMathOperators().size() > 0) {
			this.root.setAllele(MathOperator.getRandomOperator(geneGenerator.getMathOperators()));
		} else {
			this.root.setAllele(MathOperator.getRandomOperator());
		}

		this.root.setType(GeneType.MATHOP);

		// Assign values to genes
		this.assignGene(this.root);

	}

	/**
	 *	Reset the gene index
	 */
	public void resetGeneIndex() {
		this.geneIndex = 0;
	}

	/**
	 *	Get gene index
	 *
	 *	@return Gene index
	 */
	public int getGeneIndex() {
		return geneIndex;
	}

	/**
	 *	Print chromosome in preorder
	 */
	public void printPreorder() {
		this.printPreorderResurvice(this.root);
	}

	/**
	 *	Print chromosome in inorder
	 */
	public void printInorder() {
		this.printInorderResurvice(this.root);
	}

	/**
	 *	Get the length of gene in this chromosome
	 *
	 *	@return Length of gene
	 */
	public int getGeneLength() {

		if (this.root == null) {
			return 0;
		}

		return this.countGene(this.root);
	}

	/**
	 *	Compute the genes
	 *
	 *	@return Result of computation
	 */
	public double compute() {
		return this.execute(this.root, new HashMap<String, Double>());
	}

	/**
	 *	Compute the genes with symbols
	 *
	 *	@param symVal Symbols and Values
	 *
	 *	@return Result of computation
	 */
	public double compute(Map<String, Double> symVal) {
		return this.execute(this.root, symVal);
	}

	/**
	 *	Clone of this chromosome
	 *
	 *	@return Clone of this chromosome
	 */
	public Chromosome clone() {
		return new Chromosome(this.root.clone(), this.geneGenerator);
	}

	/**
	 *	Replace gene at the specified index
	 *
	 *	@param index Index of gene to be replaced
	 *	@param gene  Gene to replace
	 */
	public void crossover(int index, Gene gene) {
		if (this.getGeneLength() >= index) {
			this.geneIndex = 0;
			this.replaceGeneRecursive(this.root, index, gene);
		}
	}

	/**
	 *	Mutate gene
	 */
	public void mutate() {

		// Initialise random
		Random random = new Random();

		// Get random index of gene to mutate
		int index = random.nextInt(this.getGeneLength());

		// Get gene
		Gene gene = this.getGene(index);;

		// If gene is root, random generate only math operators
		if (index == 0) {
			gene.setType(GeneType.MATHOP);

			if (this.geneGenerator.getMathOperators().size() > 0) {
				gene.setAllele(MathOperator.getRandomOperator(this.geneGenerator.getMathOperators()));
			} else {
				gene.setAllele(MathOperator.getRandomOperator());
			}

		} else {

			if (gene.isTerminal()) {
				Gene newGene = this.geneGenerator.generate(true);

				gene.setType(newGene.getType());
				gene.setAllele(newGene.getAllele());

			} else {
				// Dice to determine if to generate terminal or non terminal gene
				boolean[] dice = {true, false};

				// New gene
				Gene newGene = this.geneGenerator.generate(dice[random.nextInt(2)]);
				newGene.setParentGene(gene.getParentGene());

				if (GeneType.isTerminal(newGene.getType())) {
					gene.replaceWith(newGene);
				} else {
					newGene.setLeftGene(gene.getLeftGene());
					newGene.setRightGene(gene.getRightGene());
					gene.replaceWith(newGene);
				}
			}

		}

	}

	/**
	 *	Get gene at the specified index
	 *
	 *	@param index Index of gene
	 *
	 *	@return Gene
	 */
	public Gene getGene(int index) {
		this.geneIndex = 0;
		return this.getGeneRecursive(this.root, index);
	}

	/**
	 *	Get root gene
	 *
	 *	@return Gene
	 */
	public Gene getRootGene() {
		return this.root;
	}

	/**
	 *	Check if chromosomes are identical
	 *
	 *	@param chromosome Chromosome to be compared
	 *
	 *	@return True if they are, false otherwise
	 */
	public boolean identical(Chromosome chromosome) {
		return this.identicalRecursive(this.root, chromosome.getRootGene());
	}

	/**
	 *	Helper Methods
	 */

	/**
	 *	Add empty gene into chromosome
	 *
	 *	Empty gene has random assigned weight, which is
	 *	used to randomly added into chromosome as in binary
	 *	tree. Less weighted genes are on the left and more weighted
	 *	genes are on the right.
	 *
	 *	@param current    Currently visiting gene
	 *	@param geneWeight Weight of gene
	 *
	 *	@return Root gene
	 */
	private Gene addEmptyGeneRecursive(Gene current, int geneWeight) {

		if (current == null) {
			Gene newGene = new Gene();
			newGene.setWeight(geneWeight);
			newGene.setParentGene(current);

			return newGene;
		}

		if (geneWeight < current.getWeight()) {
			current.setLeftGene(this.addEmptyGeneRecursive(current.getLeftGene(), geneWeight));
		} else if (geneWeight > current.getWeight()) {
			current.setRightGene(this.addEmptyGeneRecursive(current.getRightGene(), geneWeight));
		} else {
			return current; // Weight already exists.
		}

		return current;

	}

	/**
	 *	Count the number of gene in this chromosome recursively
	 *
	 *	@param current Current gene
	 *
	 *	@return Number of gene
	 */
	private int countGene(Gene current) {
		
		Gene leftGene = current.getLeftGene();
		Gene rightGene = current.getRightGene();

		int count = 1;

		if (leftGene != null) {
			count += this.countGene(leftGene);
		}

		if (rightGene != null) {
			count += this.countGene(rightGene);
		}

		return count;

	}

	/**
	 *	Traverse chromosome in preorder and print the gene allele
	 *
	 *	@param current Currently visiting gene
	 */
	private void printPreorderResurvice(Gene current) {
		
		if (current == null) {
			return;
		}

		System.out.print(current.getAllele() + " ");

		this.printPreorderResurvice(current.getLeftGene());
		this.printPreorderResurvice(current.getRightGene());

	}

	/**
	 *	Traverse chromosome in inorder and print the gene allele
	 *
	 *	@param current Currently visiting gene
	 */
	private void printInorderResurvice(Gene current) {
		
		if (current == null) {
			return;
		}

		this.printInorderResurvice(current.getLeftGene());

		System.out.print(current.getAllele() + " ");

		this.printInorderResurvice(current.getRightGene());

	}

	/**
	 *	Fix the binary tree
	 *
	 *	@param current Currently visiting gene
	 */
	private void amendment(Gene current) {
		
		if (current == null) {
			return;
		}

		if (!current.isTerminal()) {
			if (current.getLeftGene() == null) {
				current.setLeftGene(new Gene());
			} 

			if (current.getRightGene() == null) {
				current.setRightGene(new Gene());
			}
		}

		this.amendment(current.getLeftGene());
		this.amendment(current.getRightGene());
	}

	/**
	 *	Traverse preorder and assign gene's allele and type
	 *
	 *	@param current       Currently visiting gene
	 */
	private void assignGene(Gene current) {
		
		GeneGenerator geneGenerator = this.geneGenerator;

		if (current == null) {
			return;
		}

		if (current.isUnassigned()) {

			// Initialise gene
			Gene gene = null;

			// Check if current gene is terminal
			if (current.isTerminal()) {

				// Generate terminal gene
				gene = geneGenerator.generate(true);

			} else {

				// Generate non terminal gene
				gene = geneGenerator.generate();

			}

			// Assign the value
			current.setType(gene.getType());
			current.setAllele(gene.getAllele());

			// For math function, 
			// if left child is terminal, eliminate the left child
			// If left child is not terminal, then create random operator gene,
			if (gene.getType() == GeneType.MATHFUNC) {

				if (current.getLeftGene().isTerminal()) {
					current.setLeftGene(null);
				} else {
					Gene newGene = new Gene();
					newGene.setType(GeneType.MATHOP);

					if (geneGenerator.getMathOperators().size() > 0) {
						newGene.setAllele(MathOperator.getRandomOperator(geneGenerator.getMathOperators()));
					} else {
						newGene.setAllele(MathOperator.getRandomOperator());
					}

					// Link the new gene and current gene's parent
					newGene.setParentGene(current.getParentGene());

					// Link the new gene's left link with current gene's left gene
					newGene.setLeftGene(current.getLeftGene());

					// Link the new gene's right link with current gene
					newGene.setRightGene(current);

					current.setParentGene(newGene);

					current.setLeftGene(null);

					current = newGene;
				}

			}
		}

		this.assignGene(current.getLeftGene());
		this.assignGene(current.getRightGene());

	}

	/**
	 *	Execute math expression for computation
	 *
	 *	@param current Currently visiting gene
	 *	@param symVal  Symbols and values
	 *
	 *	@return Result of computation
	 */
	private double execute(Gene current, Map<String, Double> symVal) {

		// Return the value of current gene if the current gene is terminal
		if (current.isTerminal()) {

			// Check if it is symbol gene
			if (current.getType() == GeneType.SYMBOL) {
				return symVal.get(current.getAllele());
			}

			return Double.parseDouble(current.getAllele());
		}

		// Left numeric value
		double left = 0;

		// Right numeric value
		double right = 0;

		// Check if left gene exists
		if (current.getLeftGene() != null) {
			left = this.execute(current.getLeftGene(), symVal);
		}

		// Check if right gene exists
		if (current.getRightGene() != null) {
			right = this.execute(current.getRightGene(), symVal);
		}

		double result = 0;

		switch(current.getType()) {
			case GeneType.MATHOP:
				result = MathOperator.execute(current.getAllele(), left, right);
				break;

			case GeneType.MATHFUNC:
				result = MathFunction.execute(current.getAllele(), right);
				break;

			default:
				break;
		}

		return result;

	}

	/**
	 *	Recursively find the index and replace the gene
	 *
	 *	@param current 		Currently visiting gene
	 *	@param index 		Index of gene to be replaced
	 *	@param gene 	    Gene to replace
	 */
	private void replaceGeneRecursive(Gene current, int index, Gene gene) {

		if (this.geneIndex == index) {
			current.replaceWith(gene);
			return;
		}

		if (current.getLeftGene() != null) {
			this.geneIndex++;
			this.replaceGeneRecursive(current.getLeftGene(), index, gene);
		}

		if (current.getRightGene() != null) {
			this.geneIndex++;
			this.replaceGeneRecursive(current.getRightGene(), index, gene);
		}
	}

	/**
	 *	Recursively look for the gene at index
	 *
	 *	@param current Currently visiting gene
	 *	@param index   Index of gene
	 *
	 *	@return Gene at index
	 */
	private Gene getGeneRecursive(Gene current, int index) {

		if (this.geneIndex == index) {
			return current;
		}

		Gene left = null;
		Gene right = null;

		if (current.getLeftGene() != null) {
			this.geneIndex++;
			left = this.getGeneRecursive(current.getLeftGene(), index);
		}

		if (current.getRightGene() != null) {
			this.geneIndex++;
			right = this.getGeneRecursive(current.getRightGene(), index);
		}

		if (left != null) {
			return left;
		}

		if (right != null) {
			return right;
		}

		return null;
	}

	/**
	 *	Recursively check if two chromosome are identical
	 *
	 *	@param gene_x This chromosome root gene
	 *	@param gene_y Gene of chromosome to be compared
	 */
	private boolean identicalRecursive(Gene gene_x, Gene gene_y) { 

        if (gene_x == null && gene_y == null) {
            return true; 
        }
              
        if (gene_x != null && gene_y != null)  
            return (gene_x.getAllele().equals(gene_y.getAllele()) &&
            		gene_x.getType() == gene_y.getType() && 
            		identicalRecursive(gene_x.getLeftGene(), gene_y.getLeftGene()) &&
                    identicalRecursive(gene_x.getRightGene(), gene_y.getRightGene())); 
   
        return false; 
    } 

}