package ga;

import java.util.LinkedList;
import java.util.Random;

/**
 *	GeneGenerator.java
 *
 *	@author Kelvin (Zaw Moe Htat)
 */

public class GeneGenerator {

	/**
	 *	Number of gene to be generated
	 */
	private int geneLength;

	/**
	 *	Set of terminal gene types
	 */
	private LinkedList<Integer> terminalGeneTypes;

	/**
	 *	Set of non terminal gene types
	 */
	private LinkedList<Integer> nonTerminalGeneTypes;

	/**
	 *	Set of symbols
	 */
	private LinkedList<String> symbols;

	/**
	 *	Set of math operators
	 */
	private LinkedList<Integer> operators;

	/**
	 *	Set of math functions
	 */
	private LinkedList<Integer> functions;

	/**
	 *	Constructor
	 *
	 *	Initialise this generator
	 */
	public GeneGenerator() {

		// Initialise gene length
		this.geneLength = 20;
		
		// Initialise terminal gene types
		this.terminalGeneTypes = new LinkedList<Integer>();

		// Initialise non terminal gene types
		this.nonTerminalGeneTypes = new LinkedList<Integer>();

		// Initialise symbols
		this.symbols = new LinkedList<String>();

		// Initialise operators
		this.operators = new LinkedList<Integer>();

		// Initialise functions
		this.functions = new LinkedList<Integer>();

	}

	/**
	 *	Set gene length to be generated
	 *
	 *	@param geneLength
	 */
	public void setGeneLength(int geneLength) {
		this.geneLength = geneLength;
	}


	/**
	 *	Add terminal gene type
	 *
	 *	@param type Terminal gene type
	 */
	public void addTerminalGeneType(int type) {
		if (!this.terminalGeneTypes.contains(type)) {
			this.terminalGeneTypes.add(type);
		}
	}

	/**
	 *	Add non terminal gene type
	 *
	 *	@param type Non terminal gene type
	 */
	public void addNonTerminalGeneType(int type) {
		if (!this.nonTerminalGeneTypes.contains(type)) {
			this.nonTerminalGeneTypes.add(type);
		}
	}

	/**
	 *	Add symbol into the generator symbols set
	 *
	 *	@param symbol Symbol to be added
	 */
	public void addSymbol(String symbol) {
		if (!this.symbols.contains(symbol)) {
			this.symbols.add(symbol);
		}
	}

	/**
	 *	Add math operator into the operators set
	 *
	 *	@param operator Operator to be added
	 */
	public void addMathOperator(Integer operator) {
		if (!this.operators.contains(operator)) {
			this.operators.add(operator);
		}
	}

	/**
	 *	Add math function into the functions set
	 *
	 *	@param function Function to be added
	 */
	public void addMathFunction(Integer function) {
		if (!this.functions.contains(function)) {
			this.functions.add(function);
		}
	}

	/**
	 *	Get gene length
	 *
	 *	@return Gene length
	 */
	public int getGeneLength() {
		return this.geneLength;
	}

	/**
	 *	Get set of terminal gene types
	 *
	 *	@return Set of terminal gene type
	 */
	public LinkedList<Integer> getTerminalGeneTypes() {
		return this.terminalGeneTypes;
	}

	/**
	 *	Get set of non terminal gene type
	 *
	 *	@return Set of non terminal gene type
	 */
	public LinkedList<Integer> getNonTerminalGeneTypes() {
		return this.nonTerminalGeneTypes;
	}

	/**
	 *	Get set of symbols
	 *
	 *	@return Set of symbols
	 */
	public LinkedList<String> getSymbols() {
		return this.symbols;
	}

	/**
	 *	Get set of math operator
	 *
	 *	@return Set of math operators
	 */
	public LinkedList<Integer> getMathOperators() {
		return this.operators;
	}

	/**
	 *	Get set of  math function
	 *
	 *	@return Set of math functions
	 */
	public LinkedList<Integer> getMathFunctions() {
		return this.functions;
	}

	/**
	 *	Generate gene
	 *
	 *	@param terminal True for terminal gene, false for non terminal gene
	 *
	 *	@return Fully constructed gene
	 */
	public Gene generate(boolean terminal) {

		// Initialse gene
		Gene gene = new Gene();

		// Initialise random
		Random random = new Random();

		if (terminal) {

			// Roll the dice to get random terminal gene types
			int terminalGeneType = this.terminalGeneTypes.get(random.nextInt(this.terminalGeneTypes.size()));

			// Set the gene type
			gene.setType(terminalGeneType);

			// Generate allele
			switch(terminalGeneType) {
				case GeneType.SYMBOL :
					gene.setAllele(this.symbols.get(random.nextInt(this.symbols.size())));
					break;

				case GeneType.INTCONST :
					gene.setAllele(Integer.toString(random.nextInt(100) + 1));
					break;

				case GeneType.DOUBLECONST :
					gene.setAllele(Double.toString(random.nextDouble() + random.nextInt(100)));
					break;

				default:
					gene.setAllele("");
					break;
			}

		} else {

			// Roll the dice to get random non terminal gene types
			int nonTerminalGeneType = this.nonTerminalGeneTypes.get(random.nextInt(this.nonTerminalGeneTypes.size()));

			// Set the gene type
			gene.setType(nonTerminalGeneType);

			// Generate allele
			switch(nonTerminalGeneType) {
				case GeneType.MATHOP :

					// Get random math operator
					String operator = "";

					if (this.operators.size() > 0) {
						operator = MathOperator.getRandomOperator(this.operators);
					} else {
						operator = MathOperator.getRandomOperator();
					}

					gene.setAllele(operator);

					break;

				case GeneType.MATHFUNC :

					// Get random math function
					String function = "";

					if (this.functions.size() > 0) {
						function = MathFunction.getRandomFunction(this.functions);
					} else {
						function = MathFunction.getRandomFunction();
					}

					gene.setAllele(function);

					break;

				default:
					gene.setAllele("");
			}

		}

		return gene;

	}

	/**
	 *	Generate non terminal gene
	 *
	 *	@return Fully constructed gene
	 */
	public Gene generate() {
		return this.generate(false);
	}


}