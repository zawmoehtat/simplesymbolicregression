package ga;

import java.util.*;

/**
 *	Engine.java
 *
 *	This engine perform genetic algorithm
 *
 *	@author Kelvin (Zaw Moe Htat)
 */

public class Engine {

	/**
	 *	Gene generator configuration
	 */
	GeneGenerator geneGenerator;

	/**
	 *	Population of individuals
	 */
	Population population;

	/**
	 *	Dataset inputs
	 */
	LinkedList<Map<String, Double> > inputs;

	/**
	 *	Dataset outputs
	 */
	LinkedList<Double> outputs;

	/**
	 *	Dataset labels
	 */
	LinkedList<String> labels;

	/**
	 *	Default initialisation
	 */
	public Engine() {
		this.geneGenerator = null;
		this.population = null;
		this.inputs = new LinkedList<Map<String, Double> >();
		this.outputs = new LinkedList<Double>();
		this.labels = new LinkedList<String>();
	}

	/**
	 *	Initialise population
	 *
	 *	@param int Size of population
	 *	@param geneGenerator
	 */
	public void initialisePopulation(int size, GeneGenerator geneGenerator) {
		this.geneGenerator = geneGenerator;
		this.population = new Population(size, geneGenerator);
	}

	/**
	 *	Initialise population
	 *
	 *	@param int Size of population
	 */
	public void initialisePopulation(int size) {
		if (this.geneGenerator != null) {
			this.population = new Population(size, this.geneGenerator);
		}
	}

	/**
	 *	Set gene generator
	 *
	 *	@param geneGenerator
	 */
	public void setGeneGenerator(GeneGenerator geneGenerator) {
		this.geneGenerator = geneGenerator;
	}

	/**
	 *	Set labels
	 *
	 *	@param labels
	 */
	public void setLabels(LinkedList<String> labels) {
		this.labels = labels;
	}

	/**
	 *	Set Inputs
	 *
	 *	@param inputs
	 */
	public void setInputs(LinkedList<Map<String, Double> > inputs) {
		this.inputs = inputs;
	}

	/**
	 *	Set outputs
	 *
	 *	@param outputs
	 */
	public void setOutputs(LinkedList<Double> outputs) {
		this.outputs = outputs;
	}

	/**
	 *	Get Gene Generator
	 *
	 *	@return geneGenerator
	 */
	public GeneGenerator getGeneGenerator() {
		return this.geneGenerator;
	}

	/**
	 *	Get population
	 *
	 *	@return population
	 */
	public Population getPopulation() {
		return this.population;
	}

	/**
	 *	Get labels
	 *
	 *	@return Label of dataset
	 */
	public LinkedList<String> getLabels() {
		return this.labels;
	}

	/**
	 *	Get inputs
	 *
	 *	@return Inputs
	 */
	public LinkedList<Map<String, Double> > getInputs() {
		return this.inputs;
	}

	/**
	 *	Get outputs
	 *
	 *	@return Outputs
	 */
	public LinkedList<Double> getOutputs() {
		return this.outputs;
	}

	/**
	 *	Run the algorithm
	 *
	 *	@param numGeneration
	 */
	public void run(int numGeneration) {

		for (int generation = 0; generation < numGeneration; generation++) {
			for(Individual individual : this.population.getIndividuals()) {

				// Calculate the fitness
				individual.fit(this.inputs, this.outputs);

				// The individual is successfully created if fitness of this individual can be found
				while(Double.isInfinite(individual.getFitness()) || Double.isNaN(individual.getFitness())) {
					
					// Re engineer the genes of this individual
					// Make sure there is no twin
					Individual newIndividual = new Individual(geneGenerator);
					newIndividual.fit(this.inputs, this.outputs);

					while(this.population.has(newIndividual)) {
						newIndividual = new Individual(geneGenerator);
						newIndividual.fit(this.inputs, this.outputs);
					}

					individual.setChromosome(newIndividual.getChromosome());
					individual.setFitness(newIndividual.getFitness());

				}

			}

			System.out.println("Generation " + (generation + 1));
			System.out.println("=================================================");

			System.out.print("Fittest Individual: ");
			this.population.getFittestIndividual().printChromosome();
			System.out.print(" | " + this.population.getFittestIndividual().getFitness());
			System.out.println();

			System.out.print("Second Fittest Individual: ");
			this.population.getSecondFittestIndividual().printChromosome();
			System.out.print(" | " + this.population.getSecondFittestIndividual().getFitness());
			System.out.println();

			System.out.print("Least Fittest Individual: ");
			this.population.getLeastFittestIndividual().printChromosome();
			System.out.print(" | " + this.population.getLeastFittestIndividual().getFitness());
			System.out.println();

			System.out.println();


			/*System.out.println("DEBUG:: ");
			int ci = 0;
			for(Individual individual : this.population.getIndividuals()) {
				ci++;

				System.out.print("Chromosome " + ci + " : ");
				individual.printChromosome();

				System.out.println();

			} */

			// Make a baby
			// If it's twin, make it again
			Individual fittestChild = this.makeNewChild();
			
			while(this.population.has(fittestChild)) {
				fittestChild = this.makeNewChild();
			}

			// Replace the least fittest individual with child
			this.population.replaceLeastFittestIndividual(fittestChild);
			this.population.sort();

		}

	}

	/**
	 *	Make new child
	 *
	 *	- Crossover
	 *	- Mutation
	 *	- Selection
	 *
	 *	@return Fittest child
	 */
	private Individual makeNewChild() {

		// Fittest individual
		Individual fittestIndividual = this.population.getFittestIndividual();

		// Second fittest individual
		Individual secondFittestIndividual = this.population.getSecondFittestIndividual();

		// Initialise random
		Random random = new Random();

		// Get crossover point for fittest individual
		int fittestCrossoverPoint = random.nextInt(fittestIndividual.getChromosome().getGeneLength());

		// Get crossover point for second fittest individual
		int secondFittestCrossoverPoint = random.nextInt(secondFittestIndividual.getChromosome().getGeneLength());


		// Make sure crossover point are not zero
		if (fittestCrossoverPoint == 0) {
			fittestCrossoverPoint = fittestCrossoverPoint + 1;
		}

		if (secondFittestCrossoverPoint == 0) {
			secondFittestCrossoverPoint = secondFittestCrossoverPoint + 1;
		}

		// Get piece of genes from fittest individual
		Gene fittestGene = fittestIndividual.getChromosome().getGene(fittestCrossoverPoint);

		// Get piece of genes from second fittest individual
		Gene secondFittestGene = secondFittestIndividual.getChromosome().getGene(secondFittestCrossoverPoint);

		// First child
		Individual childA = fittestIndividual.clone();

		// Crossover
		childA.getChromosome().crossover(fittestCrossoverPoint, secondFittestGene);

		// Second child
		Individual childB = secondFittestIndividual.clone();

		// Crossover
		childB.getChromosome().crossover(secondFittestCrossoverPoint, fittestGene);

		// Mutation on first child
		childA.getChromosome().mutate();

		// Mutation on second child
		childB.getChromosome().mutate();

		// Put both children into the list
		LinkedList<Individual> children = new LinkedList<Individual>();
		children.add(childA);
		children.add(childB);

		// Search for fittest child
		for(Individual individual : children) {
			// Calculate the fitness
			individual.fit(this.inputs, this.outputs);

			// The individual is successfully created if fitness of this individual can be found
			while(Double.isInfinite(individual.getFitness()) || Double.isNaN(individual.getFitness())) {
				
				// Re engineer the genes of this individual
				individual.init(geneGenerator);

				// Calculate the fintess
				individual.fit(this.inputs, this.outputs);
			}

		}

		Individual fittestChild = children.get(0);

		return fittestChild;

	}



}