package ga;

import java.util.Random;
import java.util.LinkedList;

/**
 *	MathFunction.java
 *
 *	MathFunction handle the execution of mathematical function
 *	such as sin, cos, tan.
 *
 *	@author Kelvin (Zaw Moe Htat)
 */

public class MathFunction {

	/**
	 *	Index of math functions set
	 */
	public static final int SIN = 0;
	public static final int COS = 1;
	public static final int TAN = 2;

	/**
	 *	Math Operators
	 */

	public static String[] functions = {
		"sin",
		"cos",
		"tan"
	};

	/**
	 *	Encode math functions
	 *
	 *	@param function
	 *
	 *	@return -1 if not exists, function id if it does
	 */
	public static int encode(String function) {
		int id = -1;

		for(int i = 0; i < MathFunction.functions.length; i++) {
			if (MathFunction.functions[i].equals(function)) {
				id = i;
				break;
			}
		}

		return id;
	}

	/**
	 *	Decode math functions
	 *
	 *	@param id 
	 *
	 *	@return Null if not exists, operator if it does
	 */
	public static String decode(int id) {
		if (id >= functions.length || id < 0) {
			return null;
		}

		return MathFunction.functions[id];

	}

	/**
	 *	Get the random function
	 *
	 *	@return Random function from functions set
	 */
	public static String getRandomFunction() {

		// Initialise Random
		Random random = new Random();

		return MathFunction.functions[random.nextInt(MathFunction.functions.length)];
	}

	/**
	 *	Get the selective function randomly
	 *
	 *	@param selection List of custom selected functions
	 *
	 *	@return Random function from the selection list
	 */
	public static String getRandomFunction(LinkedList<Integer> selection) {

		// Initialise Random
		Random random = new Random();

		return MathFunction.functions[selection.get(random.nextInt(selection.size()))];

	}

	/**
	 *	Perform mathematical computation
	 *
	 *	@param function Math function
	 *	@param a 	    Numeric
	 *
	 *	@return Result of computation
	 */
	public static double execute(String function, double a) {

		// Perform sin function
		if (function.equals("sin")) {
			return Math.sin(a);
		}

		// Perform cos function
		if (function.equals("cos")) {
			return Math.cos(a);
		}

		// Perform tan function
		if (function.equals("tan")) {
			return Math.tan(a);
		}

		return 0;

	}

	/**
	 *	Perform mathematical computation with MathFunction index value
	 *
	 *	@param functionIndex Math function index
	 *	@param a 	    	 Numeric
	 *
	 *	@return Result of computation
	 */
	public static double execute(int functionIndex, double a) {
		return MathFunction.execute(MathFunction.functions[functionIndex], a);
	}

}