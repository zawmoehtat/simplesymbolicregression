package ga;

import java.util.*;

/**
 *	Population.java
 *
 *	Population of individuals
 *
 *	@author Kelvin (Zaw Moe Htat)
 */

public class Population {

	/**
	 *	The size of population
	 */
	private int size;

	/**
	 *	List of individuals
	 */
	private LinkedList<Individual> individuals;

	/**
	 *	Default constructor
	 */
	public Population() {
		this.size = 0;
		this.individuals = new LinkedList<Individual>();
	}

	/**
	 *	Initialise population of individuals
	 *
	 *	@param size 		 Size of population
	 *	@param geneGenerator To generate gene
	 */
	public Population(int size, GeneGenerator geneGenerator) {
		this.size = size;
		this.individuals = new LinkedList<Individual>();

		for(int i = 0; i < size; i++) {
			Individual individual = new Individual(geneGenerator);

			// Elimiate twin
			if (this.individuals.size() > 0) {
				while(this.has(individual)) {
					individual = new Individual(geneGenerator);
				}
			}
			
			this.individuals.add(individual);
		}
	}

	/**
	 *	Get list of individuals
	 *
	 *	@return List of individuals
	 */
	public LinkedList<Individual> getIndividuals() {
		return this.individuals;
	}

	/**
	 *	Get the fittest individual
	 *
	 *	@return Individual Fittest individual
	 */
	public Individual getFittestIndividual() {
		this.sort();

		if (this.individuals.size() > 0) {
			return this.individuals.get(0);
		}

		return null;
	}

	/**
	 *	Get the second fittest individual
	 *
	 *	@return Individual Second fittest individual
	 */
	public Individual getSecondFittestIndividual() {
		this.sort();

		if (this.individuals.size() > 1) {
			return this.individuals.get(1);
		}

		return null;
	}

	/**
	 *	Get the least fittest individual
	 *
	 *	@return Individual Least fittest individual
	 */
	public Individual getLeastFittestIndividual() {
		this.sort();

		if (this.individuals.size() > 0) {
			return this.individuals.getLast();
		}

		return null;
	}

	/**
	 *	Sort the population of individuals
	 */
	public void sort() {
		Collections.sort(this.individuals);
	}

	/**
	 *	Replace least fittest individual
	 *
	 *	@param child
	 */
	public void replaceLeastFittestIndividual(Individual child) {
		this.individuals.removeLast();
		this.individuals.add(child);
	}

	/**
	 *	Check if the individual is twin in this population.
	 *	i.e. The the duplicated individual
	 *
	 *	@param Individual
	 *
	 *	@return True if it is, false otherwise.
	 */
	public boolean has(Individual individual) {
		for(Individual i : this.individuals) {
			if (i.isTwin(individual)) {
				return true;
			}
		}

		return false;
	}

}