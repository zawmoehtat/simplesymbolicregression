package ga;

/**
 *	Gene.java
 *
 *	Genes are all linked together to form a chromosome.
 *	This gene represents a node of binary tree. Therefore,
 *	each gene can link up to two genes (i.e Left and Right).
 *
 *	Gene can be terminal or non terminal. Non terminal gene can link
 *	more than one gene, whereas terminal genes cannot link any other
 *	gene.
 *
 *	@author Kelvin (Zaw Moe Htat)
 */

public class Gene {

	/**
	 *	Weight of this gene
	 */
	private int weight;

	/**
	 *	Type of gene
	 */
	private int type;

	/**
	 *	Value held by the gene
	 */
	private String allele;

	/**
	 *	The gene to be linked on left side
	 */
	private Gene left;

	/**
	 *	The gene to be linked on right side
	 */
	private Gene right;

	/**
	 *	The gene that is linking this gene
	 */
	private Gene parent;

	/**
	 *	Default Constructor
	 *
	 *	Initialise gene with empty values
	 */
	public Gene() {
		this.weight = 0;
		this.type   = 0;
		this.allele = "";
		this.left   = null;
		this.right  = null;
		this.parent = null;
	}

	/**
	 *	Constructor with parameters
	 *
	 *	Initialise gene with given type and allele
	 *
	 *	@param type   Type of current gene
	 *	@param allele Value for this gene to hold
	 */
	public Gene(int type, String allele) {
		this.weight = 0;
		this.type   = type;
		this.allele = allele;
		this.left   = null;
		this.right  = null;
		this.parent = null;
	}

	/**
	 *	Setter
	 */

	/**
	 *	Set the weight of gene
	 *
	 *	@param weight Weight of gene
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

	/**
	 *	Set the type of gene.
	 *	The type of gene must be defined in GeneType
	 *	
	 *	@param type Type of gene
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 *	Set the value for this gene to hold
	 *
	 *	@param allele Value of gene
	 */
	public void setAllele(String allele) {
		this.allele = allele;
	}

	/**
	 *	Set the gene to be linked on the left side
	 *
	 *	@param left The gene on the left side
	 */
	public void setLeftGene(Gene left) {
		this.left = left;
	}

	/**
	 *	Set the gene to be linked on the right side
	 *
	 *	@param right The gene on the right side
	 */
	public void setRightGene(Gene right) {
		this.right = right;
	}

	/**
	 *	Set the gene that is linking this gene
	 *
	 *	@param parent The gene that is linking this gene
	 */
	public void setParentGene(Gene parent) {
		this.parent = parent;
	}

	/**
	 *	Getter
	 */

	/**
	 *	Get the weight of gene
	 *
	 *	@return Weight of gene
	 */
	public int getWeight() {
		return this.weight;
	}

	/**
	 *	get the type of gene.
	 *	
	 *	@return Type of gene
	 */
	public int getType() {
		return this.type;
	}

	/**
	 *	Get the value for this gene to hold
	 *
	 *	@return Value of gene
	 */
	public String getAllele() {
		return this.allele;
	}

	/**
	 *	Get the gene to be linked on the left side
	 *
	 *	@return The gene on the left side
	 */
	public Gene getLeftGene() {
		return this.left;
	}

	/**
	 *	Get the gene to be linked on the right side
	 *
	 *	@return The gene on the right side
	 */
	public Gene getRightGene() {
		return this.right;
	}

	/**
	 *	Get the gene that is currently linking to this gene
	 *
	 *	@return The gene that is linking to this gene
	 */
	public Gene getParentGene() {
		return this.parent;
	}

	/**
	 *	Clone this gene
	 *
	 *	@return New clone of this gene
	 */

	public Gene clone() {
		Gene clonedGene = new Gene(this.type, this.allele);
		
		Gene leftClonedGene = null;
		Gene rightClonedGene = null;

		if (this.left != null) {
			leftClonedGene = this.left.clone();
		}

		if (this.right != null) {
			rightClonedGene = this.right.clone();
		}

		if (leftClonedGene != null) {
			leftClonedGene.setParentGene(clonedGene);
		}

		if (rightClonedGene != null) {
			rightClonedGene.setParentGene(clonedGene);
		}

		clonedGene.setLeftGene(leftClonedGene);
		clonedGene.setRightGene(rightClonedGene);

		return clonedGene;
	}

	/**
	 *	Replace this gene with new gene
	 */

	public void replaceWith(Gene gene) {

		this.type = gene.getType();
		this.allele = gene.getAllele();
		this.left = gene.getLeftGene();
		this.right = gene.getRightGene();
		this.parent = gene.getParentGene();
	}

	/**
	 *	Check if gene is terminal.
	 *	i.e Gene doesn't have children nodes
	 *
	 *	@return True if it is terminal, false otherwise.
	 */
	public boolean isTerminal() {
		return (this.left == null && this.right == null);
	}

	/**
	 *	Check if the gene is root
	 *
	 *	i.e. there is no parent gene
	 *
	 *	@return True if it is root, false otherwise
	 */
	public boolean isRoot() {
		return (this.parent == null);
	}

	/**
	 *	Check if this gene's allele has been assigned
	 *
	 *	@return True if it is not assigned, false otherwise
	 */
	public boolean isUnassigned() {
		return (this.type == GeneType.EMPTY && this.allele.equals(""));
	}


}