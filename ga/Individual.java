package ga;

import java.util.*;

/**
 *	Individual.java
 *
 *	Individual contains its own chromosome and fitness.
 *
 *	@author Kelvin (Zaw Moe Htat)
 */

public class Individual implements Comparable<Individual> {

	/**
	 *	Chromosome of this individual
	 */
	private Chromosome chromosome;

	/**
	 *	Fitness of this individual
	 *
	 *	Fitness is measured by Mean Square Error.
	 *	The lesser the error, the fitter the individual
	 */
	private double fitness;

	/**
	 *	Default constructor
	 *
	 *	Initialise empty individual
	 */
	public Individual() {
		this.fitness = 0;
		this.chromosome = null;
	}

	/**
	 *	Initialise individual with given values
	 *
	 *	@param chromosome
	 *	@param fitness
	 */
	public Individual(Chromosome chromosome, double fitness) {
		this.chromosome = chromosome;
		this.fitness = fitness;
	}

	/**
	 *	Create individual with random genes of chromosome
	 *
	 *	@param geneGenerator To generate gene
	 */
	public Individual(GeneGenerator geneGenerator) {
		this.fitness = 0;
		this.init(geneGenerator);
	}

	/**
	 *	Initialise chromosome
	 */
	public void init(GeneGenerator geneGenerator) {
		this.chromosome = new Chromosome();
		this.chromosome.init(geneGenerator);
	}

	/**
	 *	Set chromosome of the individual
	 *
	 *	@param chromosome
	 */
	public void setChromosome(Chromosome chromosome) {
		this.chromosome = chromosome;
	}

	/**
	 *	Set fitness of individual
	 *
	 *	@param fitness
	 */
	public void setFitness(double fitness) {
		this.fitness = fitness;
	}

	/**
	 *	Get the chromosome of individual
	 *
	 *	@return Chromosome of individual
	 */
	public Chromosome getChromosome() {
		return this.chromosome;
	}

	/**
	 *	Get the fitness of individual
	 *
	 *	@return Fitness of individual
	 */
	public double getFitness() {
		return this.fitness;
	}

	/**
	 *	Find mean square error
	 *
	 *	@param inputs  Actual variables with values
	 *	@param outputs Actual output of computation
	 */
	public void fit(LinkedList<Map<String, Double> > inputs, LinkedList<Double> outputs) {

		// Prediction output
		LinkedList<Double> predOutputs = new LinkedList<Double>();

		// Compute chromosome with inputs
		for(Map<String, Double> input : inputs) {
			double predOutput = this.chromosome.compute(input);
			predOutputs.add(predOutput);
		}

		// Calculate mean square error
		int n = outputs.size();
		double sum = 0;

		for (int i = 0; i < n; i++) {
			double diff = outputs.get(i) - predOutputs.get(i);
			sum += Math.pow(diff, 2);
		}

		this.fitness = (sum / n);

	}

	/**
	 *	Clone this individual
	 *
	 *	@return Cloned individual
	 */
	public Individual clone() {
		return new Individual(this.chromosome.clone(), this.fitness);
	}

	/**
	 *	Print the string of genes of chromosome
	 */
	public void printChromosome() {
		this.chromosome.printInorder();
	}

	/**
	 *	Check if two individuals are twin
	 *
	 *	@param individual Individual to be checked
	 *	
	 *	@return True if it is, false otherwise.
	 */
	public boolean isTwin(Individual individual) {
		if (this.chromosome.identical(individual.getChromosome())) {
			return true;
		}

		if (this.fitness > 0 || individual.getFitness() > 0) {
			if (this.fitness == individual.getFitness()) {
				return true;
			}
		}

		return false;
	}

	/**
	 *	Compare two individual
	 *
	 *	@return 1 if this individual is greater than the other, 0 if equals and -1 otherwise.
	 */
	@Override
	public int compareTo(Individual individual) {
		if (this.fitness > individual.getFitness()) {
			return 1;
		} 

		if (this.fitness < individual.getFitness()) {
			return -1;
		}

		return 0;
	}
}