package ga;

/**
 *	GeneType.java
 *
 *	Type of gene
 *	=============
 *	1) UNDEF 	   : Undefined Gene Type
 *	2) SYMBOL      : Symbol such as "x", "y", "z"
 *	3) INTCONST    : Integer Constant
 *	4) DOUBLECONST : Decimal Point Constant
 *	5) MATHOP	   : Mathematical operator, such as "+", "-", "*"
 *	6) MATHFUNC	   : Mathematical Function, such as "sin", "cos", "tan"
 *
 *	@author Kelvin (Zaw Moe Htat)
 */

public class GeneType {

	public static final int EMPTY       = 0;
	public static final int SYMBOL      = 1;
	public static final int INTCONST    = 2;
	public static final int DOUBLECONST = 3;
	public static final int MATHOP      = 4;
	public static final int MATHFUNC    = 5;

	/**
	 *	Check if gene type is terminal or non terminal
	 *
	 *	@return True is if it is, false otherwise
	 */
	public static boolean isTerminal(int type) {
		if (type == GeneType.MATHOP || type == GeneType.MATHFUNC) {
			return false;
		}

		return true;
	}

}